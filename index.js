var express = require('express');
app = express();
var _und = require('underscore');
pg = require('pg');
var linker = require('./links');

app.use(function(req, res, next) {
	var start = Date.now();
	app.on('event:done', function(localreq, localres, r, err) {
		if (res._headerSent) {
			return;
		}
		myResult = {
			params: _und.extend(localreq.params, localreq.body),
			result: r,
			error: err,
			time: Date.now() - start,
			_links: linker.linksFor(req._parsedUrl)
		}
		localres.send(myResult);
	});
	next();
});

handleError = function (req, res, err, code) {
	if (! res._headerSent) {
		res.status(code || 500);
	}
	app.emit("event:done", req, res, null, err.toString());
};

doDB = function(req, res, sql, params, transform) {
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query(sql, params, function(err, result) {
			done();
			if (err) {
				handleError(req, res, err);
			} else {
				app.emit('event:done', req, res, transform(result.rows), err);
			}
		});
	});
};


var bodyParser = require('body-parser')
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));

require('./routes')(app);

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});

buildWhere = function(input, offset, prefix) {
	return _und.keys(input).map(function(name, i) {
		return prefix + name + " = $"+(i+offset);
	}).join(" AND ");
}
buildUpdate = function(input, offset) {
	return _und.keys(input).map(function(name, i) {
		return name + " = $"+(i+offset);
	}).join(", ");
}
buildIn = function(input, offset) {
	return _und.keys(input).map(function(name, i) {
		return "$"+(i+offset);
	}).join(", ");
}

filterRequiredInput = function (input, body) {
	var wrongInputs = [];
	input.forEach(function(element, index, array) {
		if(body[element] === undefined) {
			wrongInputs.push(element);
		}
	});
	return wrongInputs;
}
filterAcceptableInput = function (input, body) {
	var correctInputs = {};
	var filteredInputs = _und.filter(_und.keys(body), function(k) {
		return _und.contains(input, k);
	});
	filteredInputs.map(function(value, index) {
		correctInputs[value] = body[value];
	});
	return correctInputs;
}

dbFindById = function (req, res, table, keyNames) {
	if (! req.params.enabled) {
		req.params.enabled = 'true';
	}
	if (req.params.enabled !== 'ignore') {
		keyNames.push('enabled');
	}
	var keys = filterAcceptableInput(keyNames, req.params);
	var sql = 'SELECT * FROM ' + table +
		' WHERE ' + buildWhere(keys, 1, table + '.');

    doDB(req, res, sql, _und.values(keys), _und.first);
}

dbFindAll = function(req, res, table, joins, filterNames) {
	var args = [];
	var sql = 'SELECT ' + table + '.* FROM ' + table;
	_und.map(joins, function (val) {
		var joinTable = val[0];
		var withTable = val[1];
		var foreignKey = joinTable.substr(0, joinTable.length - 1) + '_id';

		sql += ' INNER JOIN ' + joinTable +
			' ON (' + joinTable + '.id = ' + withTable + '.' + foreignKey +
			'      AND ' +
			'     ' + joinTable + '.enabled = true) ';
	});
	if (! req.params.enabled) {
		req.params.enabled = 'true';
	}
	if (req.params.enabled !== 'ignore') {
		filterNames.push('enabled');
	}
	var filters = filterAcceptableInput(filterNames, req.params);
	if (! _und.isEmpty(filters)) {
		sql += ' WHERE ' + buildWhere(filters, 1, table + '.');
	}
	doDB(req, res, sql, _und.values(filters), _und.identity);
};

dbAdd = function(req, res, table, keyNames, fieldNames) {
	var missingInputs = filterRequiredInput(fieldNames, req.body);
	if (missingInputs.length > 0) {
		return handleError(req, res, "Missing the following inputs: " + missingInputs);
	}

	var fields = filterAcceptableInput(fieldNames, req.body);
	if (_und.isEmpty(fields) || fields.length === 0) {
		return handleError(req, res, 'No values supplied');
	}
	var keys = filterAcceptableInput(keyNames, req.params);
	var keysAndFields = _und.extend(fields, keys);

	var sql = 'INSERT INTO ' + table +
		' (' + _und.keys(keysAndFields).join(', ') + ')' +
		' VALUES (' + buildIn(keysAndFields, 1) + ')' +
		' RETURNING id';

	doDB(req, res, sql, _und.values(keysAndFields), _und.first);
};

dbUpdate = function(req, res, table, keyNames, fieldNames) {
	var fields = filterAcceptableInput(fieldNames, req.body);
	if (_und.isEmpty(fields) || fields.length === 0) {
		return handleError(req, res, 'No values supplied');
	}
	var keys = filterAcceptableInput(keyNames, req.params);
	var offsetKey = _und.keys(fields).length + 1;

	var sql = 'UPDATE ' + table +
		' SET '   + buildUpdate(fields, 1) +
		' WHERE ' + buildWhere(keys, offsetKey, table + ".") +
		' RETURNING id';

	var vals = _und.values(fields).concat(_und.values(keys));

	doDB(req, res, sql, vals, _und.first);
}
