
var _und = require('underscore');

// the endpoint hierarchy
var digraph = {
	partners:   ['groups','prompt_types'],
	groups:     ['prompts', 'responses'],
	responses:  ['selections'],
	prompts:    ['choices']
};

// identity
var makeSelf = function (path) {
	return {href: path};
};

// pagination
var makeNext = function (path, query, isMany) {
	if (isMany) {
		// TODO: generate query param for next page
		return {};
	}
	return {};
};

// children
var makeChildren = function (path, endpoint, isMany) {
	var depths = [];
	(digraph[endpoint] || []).forEach(function (el) {
		if (isMany) { // parent endpoint is findAll, child is findById
			var tpl = path + '/:id';
			depths.push({name: el, href: path + '/:id/' + el, templated: true});
		} else { // parent endpoint is findById, child is findAll
			depths.push({name: el, href: path + '/' + el, templated: false});
		}
	});
	return depths;
};

// parents
var makeParents = function (path, endpoint, parts) {
	var parents = [];
	var curr = parts.length;
	while (curr > 0) {
		var el     = parts[curr - 1];
		curr--;
		var href   = (curr === 0 ? '' : '/' + parts.slice(0, curr).join('/')) + '/' + el;
		var isMany = isNaN(el * 1);
		var name   = isMany? el : parts[curr - 1];
		if (! isMany) { // findById
			name = name.substr(0, name.length - 1);
		}
		parents.push({name: name, href: href});
	}
	return parents;
};

// siblings
// Example "/partners/1/groups" -> prompt_types
var makeSiblings = function (path, endpoint, parts) {
	var sibs = [];
	if (parts.length > 0) {
		var parent = parts[parts.length - 2];
		var parentEnd = parts.length;
		if (! isNaN(parent * 1)) {
			parent = parts[parts.length - 3];
			parentEnd--;
		}
		var parentPath = '/' + parts.slice(0, parentEnd).join('/');
		(digraph[parent] || []).forEach(function (el) {
			if (el === endpoint) return true;
			sibs.push({name: el, href: parentPath + '/' + el, templated: false});
		});
	}
	return sibs;
};

exports.linksFor = function(parsedUrl) {
	// "offset=0&limit=10" -- no leading '?'
	var query = parsedUrl.query;
	// "/partners/1/groups" -- no query params
	var path  = parsedUrl.pathname;

	var parts = path.split('/');
	// Every path has a slash at the beginning
	// spliting on slash produces an empty string in the front; ditch it.
	parts.shift();
	// If there's a trailing slash, the last el is empty string
	if (parts[parts.length - 1] === '') {
		parts.pop();
		// remove the trailing slash from the path too
		path = path.substr(0, path.length - 1);
	}

	var butlast   = parts.slice(0, parts.length - 1);
	var isMany    = true;
	var ep = last = parts[parts.length - 1];

	// If the last el is NaN, we are doing a findAll, otherwise findById
	if (! isNaN(last * 1)) {
		ep = parts[parts.length - 2];
		isMany = false;
	}

	var links = {};
	links['self'] = makeSelf(path);
	links['next'] = makeNext(path, query, isMany);
	links['children'] = makeChildren(path, ep, isMany);
	links['parents']  = makeParents(path, ep, butlast);
	links['siblings'] = makeSiblings(path, ep, butlast);
	return links;
};
