CREATE TABLE IF NOT EXISTS partners (
	id serial primary key,
	name text,
	enabled boolean default true
);

-- universal, survey, etc?
CREATE TABLE IF NOT EXISTS groups (
	id serial primary key,
	partner_id integer references partners(id),
	title text,
	description text,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

CREATE TABLE IF NOT EXISTS prompt_types (
	id serial primary key,
	partner_id integer references partners(id),
	name text,
	description text,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

CREATE TABLE IF NOT EXISTS prompts (
	id serial primary key,
	group_id integer references groups(id),
	prompt_type_id integer references prompt_types(id),
	prompt text,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

CREATE TABLE IF NOT EXISTS choices (
	id serial primary key,
	prompt_id integer references prompts(id),
	choice text,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

-- Responses
CREATE TABLE IF NOT EXISTS responses (
	id serial primary key,
	group_id integer references groups(id),
	name text default NULL,
	email text default NULL,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

CREATE TABLE IF NOT EXISTS selections (
	id serial primary key,
	response_id integer references responses(id),
	choice_id integer references choices(id),
	text text default NULL,
	enabled boolean default true,
	created_at date default CURRENT_DATE
);

