--DECLARE myid OAMENI.id%TYPE;
--INSERT INTO oameni 
--VALUES 
  --(default,'lol') 
--RETURNING id INTO myid;
--DECLARE myid partners.id%TYPE;

-- Our example partner
INSERT INTO partners ("name") VALUES ('RB3''s shop');

-- Our prompt types
INSERT INTO prompt_types (partner_id, name, description) VALUES (currval('partners_id_seq'), 'multiplechoice', 'Multiple Choice');

INSERT INTO prompt_types (partner_id, name, description) VALUES (currval('partners_id_seq'), 'textarea', 'Text Area');

INSERT INTO prompt_types (partner_id, name, description) VALUES (currval('partners_id_seq'), 'radio', 'Radio Buttons');

INSERT INTO prompt_types (partner_id, name, description) VALUES (currval('partners_id_seq'), 'checkbox', 'Check boxes');

-- Review Group
INSERT INTO groups (partner_id, title, description) VALUES (currval('partners_id_seq'), 'review', 'example review system');

INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Overall Rating', (select id from prompt_types where name = 'multiplechoice'));

INSERT INTO choices (prompt_id, choice) VALUES
	(currval('prompts_id_seq'), '1'),
	(currval('prompts_id_seq'), '2'),
	(currval('prompts_id_seq'), '3'),
	(currval('prompts_id_seq'), '4'),
	(currval('prompts_id_seq'), '5');


INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Your review headline', (select id from prompt_types where name = 'textarea'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'E.g. Excellent Product...');


INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Describe your product experience', (select id from prompt_types where name = 'textarea'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'Describe your overall product experience (Required)');


INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Would you recommend this product to a friend?', (select id from prompt_types where name = 'radio'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'yes'), (currval('prompts_id_seq'), 'no');

-- Q & A example
INSERT INTO groups (partner_id, title, description) VALUES (currval('partners_id_seq'), 'qa', 'example q and a system');

INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Ask a question', (select id from prompt_types where name = 'textarea'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'Question goes here');

INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Answer this question!', (select id from prompt_types where name = 'textarea'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'Answer goes here');


-- Doodle example
INSERT INTO groups (partner_id, title, description) VALUES (currval('partners_id_seq'), 'doodle', 'example doodle scheduler');

INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Name', (select id from prompt_types where name = 'textarea'));

INSERT INTO choices (prompt_id, choice) VALUES (currval('prompts_id_seq'), 'Your name');

INSERT INTO prompts (group_id, prompt, prompt_type_id) VALUES (currval('groups_id_seq'), 'Select available dates', (select id from prompt_types where name = 'checkbox'));

INSERT INTO choices (prompt_id, choice) VALUES
	(currval('prompts_id_seq'), '01/27'),
	(currval('prompts_id_seq'), '01/28');

INSERT INTO responses (group_id, name, email) VALUES (currval('groups_id_seq'), 'John Doe', 'john.doe@answers.com');

INSERT INTO selections (response_id, choice_id, text) VALUES (currval('responses_id_seq'), currval('choices_id_seq'), '');

INSERT INTO choices (prompt_id, choice) VALUES
	(currval('prompts_id_seq'), '02/04'),
	(currval('prompts_id_seq'), '02/05');

INSERT INTO selections (response_id, choice_id, text) VALUES (currval('responses_id_seq'), currval('choices_id_seq'), '');
