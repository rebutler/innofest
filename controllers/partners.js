var _und = require('underscore');
var table = 'partners';

exports.findAll = function(req, res) {
	dbFindAll(req, res, table, [], []);
};

exports.findById = function(req, res) {
	dbFindById(req, res, table, ['id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, [], ['name', 'enabled']);
};

exports.update = function(req, res) {
	dbUpdate(req, res, table, ['id'], ['name', 'enabled']);
};

exports.delete = function(req, res) {
	req.body.enabled = 'false';
	dbUpdate(req, res, table, ['id'], ['enabled']);
}
