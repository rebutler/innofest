var _und = require('underscore');
var table = 'responses';

exports.findAll = function(req, res) {
	var joins = [
		['groups', table],
		['partners', 'groups']
	];
	dbFindAll(req, res, table, joins, ['group_id']);
};

exports.findById = function(req, res){
	dbFindById(req, res, table, ['group_id', 'id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, ['group_id', 'id'], ['name', 'email', 'enabled']);
};

exports.update = function(req, res) {
	dbUpdate(req, res, table, ['group_id', 'id'], ['name', 'email', 'enabled']);
};

exports.delete = function(req, res) {
    req.body.enabled = 'false';
    dbUpdate(req, res, table, ['group_id', 'id'], ['enabled']);
}
