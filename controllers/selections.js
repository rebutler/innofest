var _und = require('underscore');
var table = 'selections';

// Todo - prompt scores - join up
exports.findAll = function(req, res) {
	var joins = [
		['responses', table],
		['groups', 'responses'],
		['partners', 'groups']
	];
	dbFindAll(req, res, table, joins, ['response_id']);
};

exports.findById = function(req, res){
	dbFindById(req, res, table, ['response_id', 'id']);
};

exports.add = function(req, res) {
    if (! req.body.enabled) {
        req.body.enabled = 'true'; // default value
    }
    dbAdd(req, res, table, ['response_id'], ['choice_id', 'text', 'enabled']);
};

exports.update = function(req, res) {
    dbUpdate(req, res, table, ['response_id', 'id'], ['choice_id', 'text', 'enabled']);
};

exports.delete = function(req, res) {
    req.body.enabled = 'false';
    dbUpdate(req, res, table, ['response_id', 'id'], ['enabled']);
}
