var _und = require('underscore');
var table = 'prompt_types';

// Todo - prompt scores - join up
exports.findAll = function(req, res) {
	var joins = [['partners', table]];
	dbFindAll(req, res, table, joins, ['partner_id']);
};

exports.findById = function(req, res){
	dbFindById(req, res, table, ['partner_id', 'id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, ['partner_id'], ["name", "description"]);
};

exports.update = function(req, res) {
	dbUpdate(req, res, table, ['partner_id', 'id'], ["description", "name"]);
};

exports.delete = function(req, res) {
	req.body.enabled = 'false';
    dbUpdate(req, res, table, ['partner_id', 'id'], ['enabled']);
}
