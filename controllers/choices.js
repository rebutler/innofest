var _und = require('underscore');
var table = 'choices';

// Todo - prompt scores - join up
exports.findAll = function(req, res) {
	var joins = [
		['prompts', table],
		['groups', 'prompts'],
		['partners', 'groups']
	];
	dbFindAll(req, res, table, joins, ['prompt_id']);
};

exports.findById = function(req, res){
	dbFindById(req, res, table, ['prompt_id', 'id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, ['prompt_id'], ["choice"]);
};

exports.update = function(req, res) {
	dbUpdate(req, res, table, ['prompt_id', 'id'], ['choice', 'enabled']);
};

exports.delete = function(req, res) {
	req.body.enabled = 'false';
	dbUpdate(req, res, table, ['prompt_id', 'id'], ['enabled']);
}
