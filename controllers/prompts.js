var _und = require('underscore');
var table = 'prompts';

// Todo - prompt scores - join up
exports.findAll = function(req, res) {
	var joins = [['groups', 'prompts'], ['partners', 'groups']];
	dbFindAll(req, res, table, joins, ['group_id']);
};
exports.findById = function(req, res){
	dbFindById(req, res, table, ['group_id', 'id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, ['group_id'], ["prompt", "prompt_type_id"]);
};
exports.update = function(req, res) {
	dbUpdate(req, res, table, ['group_id', 'id'], ['prompt', 'prompt_type_id', 'enabled']);
};
exports.delete = function(req, res) {
	req.body.enabled = 'false';
	dbUpdate(req, res, table, ['group_id', 'id'], ['enabled']);
};

exports.findPCs = function(req, res) {
	var f = function(rows) {
		compound = {};
		rows.forEach(function(e) {
			if (! compound[e.prompt_id]) {
				compound[e.prompt_id] = {
					"prompt_id": e.prompt_id,
					"group_id": e.group_id,
					"prompt_type_id": e.prompt_type_id,
					"choices": []
				};
			}
			if (e.choice_id) {
				compound[e.prompt_id]["choices"].push({"choice_id": e.choice_id, "choice": e.choice});
			}
		});
		return _und.values(compound);
	}
	var sql = "SELECT q.id as prompt_id, q.group_id, q.prompt_type_id, a.id AS choice_id, a.choice " +
		" FROM prompts q LEFT JOIN choices a ON a.prompt_id = q.id " +
		" WHERE q.group_id = $1 ";
	doDB(req, res, sql, [req.params.group_id], f);
};
