var _und = require('underscore');
var table = 'groups';

exports.findAll = function(req, res) {
	dbFindAll(req, res, table, [['partners', 'groups']], ['partner_id']);
};

exports.findById = function(req, res) {
	dbFindById(req, res, table, ['partner_id', 'id']);
};

exports.add = function(req, res) {
	if (! req.body.enabled) {
		req.body.enabled = 'true'; // default value
	}
	dbAdd(req, res, table, ['partner_id'], ["title", "description"]);
};

exports.update = function(req, res) {
	dbUpdate(req, res, table, ['partner_id', 'id'], ["title", "description"]);
};

exports.delete = function(req, res) {
    req.body.enabled = 'false';
    dbUpdate(req, res, table, ['id'], ['enabled']);
}
