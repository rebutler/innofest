module.exports = function(app) {

	var home = require('./controllers/home');
	app.get('/', home.base);

	var partners = require('./controllers/partners');
	app.get('/partners', partners.findAll);
	app.get('/partners/:id', partners.findById);
	app.post('/partners', partners.add);
	app.put('/partners/:id', partners.update);
	app.delete('/partners/:id', partners.delete);

	var groups = require('./controllers/groups');
	app.get('/partners/:partner_id/groups', groups.findAll);
	app.get('/partners/:partner_id/groups/:id', groups.findById);
	app.post('/partners/:partner_id/groups', groups.add);
	app.put('/partners/:partner_id/groups/:id', groups.update);
	app.delete('/partners/:partner_id/groups/:id', groups.delete);

	var prompt_types = require('./controllers/prompt_types');
	app.get('/partners/:partner_id/prompt_types', prompt_types.findAll);
	app.get('/partners/:partner_id/prompt_types/:id', prompt_types.findById);
	app.post('/partners/:partner_id/prompt_types', prompt_types.add);
	app.put('/partners/:partner_id/prompt_types/:id', prompt_types.update);
	app.delete('/partners/:partner_id/prompt_types/:id', prompt_types.delete);

	var prompts = require('./controllers/prompts');
	app.get('/partners/:partner_id/groups/:group_id/prompts', prompts.findAll);
	app.get('/partners/:partner_id/groups/:group_id/prompts/:id', prompts.findById);
	app.post('/partners/:partner_id/groups/:group_id/prompts', prompts.add);
	app.put('/partners/:partner_id/groups/:group_id/prompts/:id', prompts.update);
	app.get('/partners/:partner_id/groups/:group_id/promptchoices', prompts.findPCs);
	app.delete('/partners/:partner_id/groups/:group_id/prompts/:id', prompts.delete);

	var choices = require('./controllers/choices');
	app.get('/partners/:partner_id/groups/:group_id/prompts/:prompt_id/choices', choices.findAll);
	app.get('/partners/:partner_id/groups/:group_id/prompts/:prompt_id/choices/:id', choices.findById);
	app.post('/partners/:partner_id/groups/:group_id/prompts/:prompt_id/choices', choices.add);
	app.put('/partners/:partner_id/groups/:group_id/prompts/:prompt_id/choices/:id', choices.update);
	app.delete('/partners/:partner_id/groups/:group_id/prompts/:prompt_id/choices/:id', choices.delete);

	var responses = require('./controllers/responses');
	app.get('/partners/:partner_id/groups/:group_id/responses', responses.findAll);
	app.get('/partners/:partner_id/groups/:group_id/responses/:id', responses.findById);
	app.post('/partners/:partner_id/groups/:group_id/responses', responses.add);
	app.put('/partners/:partner_id/groups/:group_id/responses/:id', responses.update);
	app.delete('/partners/:partner_id/groups/:group_id/responses/:id', responses.delete);

	var selections = require('./controllers/selections');
	app.get('/partners/:partner_id/groups/:group_id/responses/:response_id/selections', selections.findAll);
	app.get('/partners/:partner_id/groups/:group_id/responses/:response_id/selections/:id', selections.findById);
	app.post('/partners/:partner_id/groups/:group_id/responses/:response_id/selections', selections.add);
	app.put('/partners/:partner_id/groups/:group_id/responses/:response_id/selections/:id', selections.update);
	app.delete('/partners/:partner_id/groups/:group_id/responses/:response_id/selections/:id', selections.delete);

}
